// ==UserScript==
// @name        Moodle compact activities
// @description Inserts CSS to make activities in courses take less space

// @version     1.0
// @license     MIT
// @author      Syndamia
//
// @source      https://gitlab.com/Syndamia/userscripts/-/blob/main/moodle-compact-activities.js
// @supportURL  https://gitlab.com/Syndamia/userscripts/-/issues
// @homepage    https://gitlab.com/Syndamia/userscripts/
// @icon        https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Moodle-logo.svg/1920px-Moodle-logo.svg.png
//
// @namespace   Syndamia
// @match *://*learn.fmi.uni-sofia.bg/*
// @run-at document-body
// ==/UserScript==

var oldStyle = document.createElement('style');
oldStyle.textContent = `
.activity-item {
	padding: 0 !important;
	border: none !important;
}

.activityiconcontainer {
	width: initial;
	height: initial;
	padding: 0.2em !important;
}

.section .activity {
	padding: 0.1em 0;
}

.description .course-description-item {
  border-left: 0.4em solid black;
}
.description .course-description-item:first-child {
  margin-top: 0;
  padding: 0.5em 1em;
}
.description .course-description-item:last-child {
  margin-bottom: 1.1em;
}
`;
document.head.append(oldStyle);
