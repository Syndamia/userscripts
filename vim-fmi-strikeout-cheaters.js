// ==UserScript==
// @name        vim-fmi зачеркване на чиитъри
// @description Всички чиитъри се зачеркват в таблицата. Чиитър е всеки който ползва системния clipboard или вмъква съдържанието от файл (read команда).
//
// @version     1.0
// @license     MIT
// @author      Syndamia
//
// @source      https://gitlab.com/Syndamia/userscripts/-/blob/main/vim-fmi-strikeout-cheaters.js
// @supportURL  https://gitlab.com/Syndamia/userscripts/-/issues
// @homepage    https://gitlab.com/Syndamia/userscripts/
// @icon        https://vim-fmi.bg/favicon.png
//
// @namespace   Syndamia
// @match *://*vim-fmi.bg/tasks/*
// @run-at document-end
// ==/UserScript==

const inAllSol = window.location.pathname.endsWith('solutions');

/* Helper functions */
function getScore(elem) {
    return parseInt(elem.children[2 + inAllSol].innerText);
}
function getSolution(elem) {
    return elem.children[1 + inAllSol].innerText;
}
function getDate(elem) {
    return elem.children[3 + inAllSol].innerText;
}

/* Get data */
const tableBody = document.getElementsByClassName('solutions')[0].children[1];

  // Since you can sort the (/solutions page) table in different ways,
  // we'll need to preserve the original indecies of elements
var rows = [];
for (let i = 0; i < tableBody.children.length; i++) {
    rows.push([tableBody.children[i], i]);
}

/* Sort by score and then by date */
rows.sort(function (a, b) {
    a = a[0]; b = b[0];
    var aInt = getScore(a), bInt = getScore(b);
    return aInt > bInt || (aInt == bInt && getDate(a) > getDate(b));
});

/* Every following solution that is the same as the current one, or
 * if a solution pastes content from the system clipboard, or
 * if a solution uses the read command,
 * it is deemed to be "cheated", and a special style is applied to it */
function usesClipboard(rowElem) {
    return getSolution(rowElem[0]).match(/("[+*](|[gz\[\]])(p|P|<MiddleMouse>)|:pu[ t]|:r( |ead))/);
}
function applyCheatedStyle(rowElem) {
    rowElem[0].style.textDecoration = 'line-through';
    rowElem[0].style.fontStyle = 'italic';
    rowElem[0].style.opacity = '0.5';
    return rowElem;
}

if (usesClipboard(rows[0])) {
    rows[0] = applyCheatedStyle(rows[0]);
}

for (let i = rows.length - 1; i > 0; i--) {
    if ((getScore(rows[i][0]) == getScore(rows[i-1][0]) && getSolution(rows[i][0]) == getSolution(rows[i-1][0]))
        || usesClipboard(rows[i]))
    {
        rows[i] = applyCheatedStyle(rows[i]);
    }
}

/* Return the original sorting of data */
if (inAllSol) {
    rows.sort(function (a, b) {
        return a[1] > b[1];
    });
}

tableBody.replaceChildren(...rows.map(r => r[0]));
