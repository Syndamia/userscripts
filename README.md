# Userscripts

A wide collection of the (useful) userscripts I've made. The only that isn't here is [min-youtube-element-blocker](https://github.com/Syndamia/min-youtube-element-blocker) since it has it's own repository.
