// ==UserScript==
// @name        vim-fmi точки
// @description Добавя брой натиснати клавиши във всяка страница на решения
//
// @version     1.4
// @license     MIT
// @author      Syndamia
//
// @source      https://gitlab.com/Syndamia/userscripts/-/blob/main/vim-fmi-keystrokes.js
// @supportURL  https://gitlab.com/Syndamia/userscripts/-/issues
// @homepage    https://gitlab.com/Syndamia/userscripts/
// @icon        https://vim-fmi.bg/favicon.png
//
// @namespace   Syndamia
// @match *://*vim-fmi.bg/tasks/*
// @run-at document-end
// ==/UserScript==

if (window.location.pathname.endsWith('solutions')) {
    /* Add scores for each solution */
    document.getElementsByTagName('tbody')[0].childNodes.forEach((x) => {
        if (x.tagName != 'TR') return;
	    var countElem = document.createElement('div');
	    countElem.style.display = 'inline-block';
        countElem.style.fontWeight = 'bold';
        countElem.innerText = x.children[1].children[0].children.length;
	    x.children[1].insertBefore(countElem, x.children[1].firstChild);
    });

    /* Sorting */
    var sortBtn = document.createElement('div');
    sortBtn.classList.add('action');
    sortBtn.innerText = 'Сортирай по решения';
    document.getElementsByClassName('main')[0].children[2].appendChild(sortBtn);

    function getScore(elem) {
        return (elem) ? parseInt(elem.children[1].children[0].innerText) : '';
    }
    function getSolution(elem) {
        return (elem) ? elem.children[1].children[1].innerText : '';
    }
    function getDate(elem) {
        return (elem) ? elem.children[2].innerText : '';
    }

    sortBtn.addEventListener('click', function () {
        var rows = [...document.getElementsByClassName('solutions')[0].children[1].children];
        rows.sort(function (a, b) {
            var aInt = getScore(a), bInt = getScore(b);
            return aInt > bInt || (aInt == bInt && getDate(a) > getDate(b));
        });

        /* Every following solution that is the same as the current one, or
         * if a solution pastes content from the system clipboard, or
         * if a solution uses the read command,
         * it is deemed to be "cheated", and is put at the bottom of the list */
        for (var i = rows.length - 1; i >= 0; i--) {
            if ((getScore(rows[i]) == getScore(rows[i-1]) && getSolution(rows[i]) == getSolution(rows[i-1]))
                || getSolution(rows[i]).match(/("[+*](|[gz\[\]])(p|P|<MiddleMouse>)|:pu[ t]|:r( |ead))/))
            {
                var cheated = rows.splice(i, 1)[0];
                cheated.style.textDecoration = 'line-through';
                cheated.style.fontStyle = 'italic';
                cheated.style.opacity = '0.5';
                rows.push(cheated);
            }
        }

        document.getElementsByClassName('solutions')[0].children[1].replaceChildren(...rows);
    });
}
else {
    /* Add scores for each solution */
    document.getElementsByTagName('ul')[3].childNodes.forEach((x) => {
        if (x.tagName != 'LI') return;
        var countElem = document.createElement('span');
        countElem.style.verticalAlign = 'middle';
        countElem.innerText = x.children[0].children.length;
        x.insertBefore(countElem, x.firstChild);
    });
}
