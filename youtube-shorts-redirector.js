// ==UserScript==
// @name        YouTube shorts redirector
// @description Redirects all youtube shorts videos to normal video pages
//
// @version     1.0
// @license     MIT
// @author      Syndamia
//
// @source      https://gitlab.com/Syndamia/userscripts/-/blob/main/youtube-shorts-redirector.js
// @supportURL  https://gitlab.com/Syndamia/userscripts/-/issues
// @homepage    https://gitlab.com/Syndamia/userscripts/
// @icon        https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Youtube_shorts_icon.svg/193px-Youtube_shorts_icon.svg.png
//
// @namespace   Syndamia
// @match       *://*.youtube.com/*
// @run-at      document-end
// ==/UserScript==

function redirect() {
    if (window.location.pathname.indexOf('shorts') !== 1) {
        return;
    }

    const loc = 'sh_' + window.location.pathname.substr(8);

    if (localStorage.getItem(loc) === 'true') {
        return;
    }

    const latest = localStorage.getItem('sh_LATEST');
    if (latest) {
        localStorage.removeItem(latest);
    }

    localStorage.setItem(loc, true);
    localStorage.setItem('sh_LATEST', loc);
    window.location = window.location.href.replace("youtube.com/shorts/", "youtube.com/watch?v=");
}

const observer = new MutationObserver(redirect);
observer.observe(document.getElementsByTagName('ytd-app')[0], { childList: true, attributes: true, subtree: true, });
