// ==UserScript==
// @name Invidious Instance Cleanup
// @description Updates some Invidious instance's stylings to be a bit nicer to use
//
// @version     1.0
// @license     MIT
// @author      Syndamia
//
// @source      https://gitlab.com/Syndamia/userscripts/-/blob/main/invidious-instance-cleanup.js
// @supportURL  https://gitlab.com/Syndamia/userscripts/-/issues
// @homepage    https://gitlab.com/Syndamia/userscripts/
// @icon        https://upload.wikimedia.org/wikipedia/commons/a/a0/Invidious-logo.svg
//
// @namespace   Syndamia
// @match *://inv.riverside.rocks/*
// @match *://yewtu.be/*
// @match *://vid.puffyan.us/*
// @run-at document-start
// ==/UserScript==

var style = document.createElement('style')

style.textContent = `
body {
  overflow-x: hidden;
}

#player-container > div {
  width: 80% !important;
}
#contents > .pure-g:nth-of-type(4) > .pure-u-lg-1-5:nth-of-type(3) {
  margin-top: -57rem;
  z-index: 1000;
  position: relative;
}
none > #contents > .h-box:nth-child(2) {
  display: none;
}
.playlist-restricted {
  height: 47em !important;
}
`

document.head.appendChild(style)
