// ==UserScript==
// @name        Пействане в ОББ сайта
// @description Позволява да поставиш текст от клипборда в ОББ полета
//
// @version     1.0
// @license     MIT
// @author      Syndamia
//
// @source      https://gitlab.com/Syndamia/userscripts/-/blob/main/
// @supportURL  https://gitlab.com/Syndamia/userscripts/-/issues
// @homepage    https://gitlab.com/Syndamia/userscripts/
// @icon        https://ebb.ubb.bg/web/images/faviconNEW.png
//
// @namespace   Syndamia
// @match *://ebb.ubb.bg/*
// @run-at document-end
// ==/UserScript==

for (var el of document.getElementsByTagName("input")) {
    el.onpaste = '';
}
