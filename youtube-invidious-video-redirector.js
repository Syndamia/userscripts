// ==UserScript==
// @name        YouTube to Invidious video redirector
// @description When opening YouTube videos, redirect to an Invidious instance, but when going back from instance, don't redirect again
//
// @version     1.1
// @license     MIT
// @author      Syndamia
//
// @source      https://gitlab.com/Syndamia/userscripts/-/blob/main/youtube-invidious-video-redirector.js
// @supportURL  https://gitlab.com/Syndamia/userscripts/-/issues
// @homepage    https://gitlab.com/Syndamia/userscripts/
// @icon        https://upload.wikimedia.org/wikipedia/commons/a/a0/Invidious-logo.svg
//
// @namespace   Syndamia
// @match       *://www.youtube.com/*
// @run-at      document-start
// ==/UserScript==

const invidiousInstance = 'yewtu.be';



const ytdObserver = new MutationObserver(function(mutations) {
    // Detect when ytd-app element is present
    if (document.getElementsByTagName('ytd-app').length > 0) {
        // Attach the redirection function to it
        const observer = new MutationObserver(redirect);
        observer.observe(document.getElementsByTagName('ytd-app')[0], { childList: true, attributes: true, subtree: true, });
        ytdObserver.disconnect();
    }
});

ytdObserver.observe(document, {attributes: false, childList: true, characterData: false, subtree:true});

// Opening YouTube links in YouTube doesn't reload the page, but we know that if the page changes, ytd-app (element that contains the UI) will also change
// so, when ytd-app changes, we check if the link has changed, and redirect
function redirect() {
    const latest = localStorage.getItem('LATEST'), loc = window.location.search.substr(3);

    if (loc !== latest) {
        localStorage.removeItem(latest);
        localStorage.removeItem('LATEST');
    }

    if (window.location.pathname === '/' || localStorage.getItem(loc) === 'true') {
        return;
    }

    localStorage.setItem(loc, true);
    localStorage.setItem('LATEST', loc);
    window.location.host = invidiousInstance;
}
