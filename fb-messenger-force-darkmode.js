// ==UserScript==
// @name        Facebook Messenger Force Dark Mode
// @description "Forcefully" applies the dark mode style to messenger (for use in sandboxed environments like Rambox)
//
// @version     Indev 0.2
// @license     MIT
// @author      Syndamia
//
// @source      https://gitlab.com/Syndamia/userscripts/-/blob/main/fb-messenger-force-darkmode.js
// @supportURL  https://gitlab.com/Syndamia/userscripts/-/issues
// @homepage    https://gitlab.com/Syndamia/userscripts/
// @icon        https://upload.wikimedia.org/wikipedia/commons/b/be/Facebook_Messenger_logo_2020.svg
//
// @namespace   Syndamia
// @match       *://*.messenger.com/*
// @run-at      document-start
// ==/UserScript==

document.querySelector('html').classList.add('__fb-dark-mode');

document.onload = function() {
    var lights = document.getElementsByClassName("__fb-light-mode");
    while (lights.length) {
        lights[0].classList.remove("__fb-light-mode");
    }
}

var style = document.createElement('style');
style.type = 'text/css';
style.innerHTML = `
:root {
        --header-height: 0px;
	--base: #000;
	--base-ninety: rgba(255, 255, 255, 0.9);
	--base-seventy-five: rgba(255, 255, 255, 0.75);
	--base-seventy: rgba(255, 255, 255, 0.7);
	--base-fifty: rgba(255, 255, 255, 0.5);
	--base-fourty: rgba(255, 255, 255, 0.4);
	--base-thirty: rgba(255, 255, 255, 0.3);
	--base-twenty: rgba(255, 255, 255, 0.2);
	--base-five: rgba(255, 255, 255, 0.05);
	--base-ten: rgba(255, 255, 255, 0.1);
	--base-nine: rgba(255, 255, 255, 0.09);
	--container-color: #323232;
	--container-dark-color: #1e1e1e;
	--list-header-color: #222;
	--blue: #0084ff;
	--selected-conversation-background: linear-gradient(hsla(209, 110%, 45%, 0.9), hsla(209, 110%, 42%, 0.9));
}

.oo9gr5id {
color: #e6e6e6;
}
.x1xr0vuk {
background-color: #3d3d3d !important;
}

/* Better scrollbar */

/* width */
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}

/* Top bar: App menu button color */
[aria-label="Settings, help and more"] .a8c37x1j.ms05siws.hwsy1cff.b7h9ocf4 {
	fill: currentColor !important;
	color: var(--primary-text) !important;
}

/* Top bar: New message button color */
[aria-label="New Message"] .a8c37x1j.ms05siws.hwsy1cff.b7h9ocf4 {
	fill: currentColor !important;
	color: var(--primary-text) !important;
}

/* Preferences: Icon color */
[aria-label="Preferences"] .a8c37x1j.ms05siws.hwsy1cff.b7h9ocf4 {
	fill: currentColor !important;
	color: var(--primary-text) !important;
}

/* Menu: Icon color in menu */
[role="menu"] .a8c37x1j.ms05siws.hwsy1cff.b7h9ocf4 {
	fill: currentColor !important;
	color: var(--primary-text) !important;
}

/* Right sidebar: Icon color */
.rq0escxv.l9j0dhe7.du4w35lb.j83agx80.cbu4d94t.g5gj957u.f4tghd1a.ifue306u.kuivcneq.t63ysoy8 .a8c37x1j.ms05siws.hwsy1cff.b7h9ocf4 {
	fill: currentColor !important;
	color: var(--primary-text) !important;
}

/* Contact list: delivered icon color */
.aahdfvyu [role="grid"] .a8c37x1j.ms05siws.hwsy1cff.b7h9ocf4 {
	fill: currentColor !important;
	color: var(--primary-text) !important;
}
body::-webkit-scrollbar {
  display: none;
}
`
document.getElementsByTagName("head")[0].appendChild( style );

/* Fixing light mode not being present */

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function fix() {
    while(true) { 
        var lights = document.getElementsByClassName("__fb-light-mode");
        while (lights.length) {
            lights[0].classList.remove("__fb-light-mode");
        }
        await sleep(1000);
    }
}

fix();
